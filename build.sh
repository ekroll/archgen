#!/usr/bin/sh
pacman -Syu --noconfirm
pacman -S --noconfirm archiso mkinitcpio-archiso
pacman -Scc --noconfirm
rm $(ls | grep *.iso)
workdir=/tmp/archgen-build
rm -rfd ${workdir}
mkdir -p ${workdir}
mkarchiso -v -w ${workdir} -o . ./iso
