# ArchGen (Under development)
A simple modified arch installation image prompting for a git repo url after shell login. \
The repo needs to have a install.sh file at root that will be run after cloning. \
The intended usage of this is automatic installation of arch using home-made scripts. \
That way we can use the same ISO, but for vastly different setups and purposes.

## Tutorial
1. `./build.sh` builds the ISO
1. `./run.sh` runs the ISO in qemu (optional)
1. `sudo dd if=ISOTHINGY of=/dev/USBTHINGY` creates installation medium

## Or download it
(When I get a pipeline running)
